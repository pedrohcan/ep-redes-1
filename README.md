# Instruções para servidor executar o projeto:

## Necessário:
  * PHP 7.2
  * MySQL
  * Uma base de dados criada, aconselhamos o nome *chat_sis*.

## Comunicação Peer to Peer para chat:
```
cd ./peerjs/
npm install peer
cd node_modules/peer/bin
node peerjs --port 9000 --key peerjs
```
Este socket roda na porta 9000.


## Comunicação via sockets para usuários (emissão de informações via servidor):
```
cd ./socketjs/
npm install
node index.js
```

Este socket roda na porta 3000.

## Comunicação via sockets para OBDs
```
cd ./socketphp/
composer install
php console
```

Este socket roda na porta 38000.

Necessário alterar o arquivo *socketphp/src/Command/Connection.php* e colocar as configurações de conexão com a base de dados:
```
private const DB_HOST = 'localhost';
private const DB_NAME = 'chat_sis';
private const DB_USER = 'root';
private const DB_PASSWORD = 'root';
```

## Aplicação WEB:
```
cd ./web/
composer install
yarn install
yarn encore production
php bin/console server:run
```

Altere seu arquivo .ENV:
```
DATABASE_URL=mysql://root:root@127.0.0.1:3306/chat_sis
```

Execute as migrations para a base de dados:
```
php bin/console doctrine:migrations:migrate
```

## Simulãção de viagem de carro:
```
cd ./reactphp/
composer install
php app.php
```
