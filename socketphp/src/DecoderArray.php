<?php

const BIN_FLAG_CHAR = 0xF8;
const MIN_PACKET_LEN = 22;
const PROTOCOL_VERSION = 0x01;
const BIN_ESCAPE_CHAR = 0xF7;
const ACK_FLAG = 0xFE;

/**
 *    @codeCoverageIgnore
 */
class DecoderArray
{
    public static function isBinary($str)
    {
        return preg_match('~[^\x20-\x7E\t\r\n]~', $str) > 0;
    }

    /**
    * @var $data contém o pacode em binário
    * return $ack pacote a ser mandado de volta para o obd, caso necessário
    */
    public static function dataDecode($data)
    {
        if (self::isBinary($data)) {
            $hex = bin2hex($data);
        } else {
            $hex = $data;
        }
        if (strlen($hex) % 2 != 0) {
            //echo("Data format error!");
            return;
        }

        $binData = self::hexToStrArray($hex);

        $ack = self::dataBinAcknowledgement($binData);

        $array = [];

        //DataDecode($buffer)
        //echo("Starting decode...\r\n");
        $offset = 0;
        while ($offset + MIN_PACKET_LEN < count($binData)) {
            $endPos = array_search(BIN_FLAG_CHAR, array_slice($binData, $offset+1, null, true));//Get binary frame end flag position
            if (!$endPos || ($endPos - $offset) < 12) {
                $offset++;
                continue;
            }

            $binFrame = array_slice($binData, $offset + 1, $endPos - $offset - 1);
            $dataLen = self::binFrameFormatCheck($binFrame);
            if ($dataLen <= 0) {
                //echo("CRC check error\r\n");
                $offset++;
                continue;
            }
            $array[] = self::binFrameDecode($binFrame, $dataLen);
            $offset = $endPos + 1;
        }

        //echo("Decode finished!\r\n");

        return $array;
    }

    public static function hexToStrArray($hex)
    {
        $hex = strtoupper($hex);
        //$binData é um arranjo em hex dos dados
        $hexArray = str_split($hex, 2);

        return self::hexStringDec($hexArray);
    }

    public static function binDataPacket($ackData)
    {
        $packet = array(BIN_FLAG_CHAR);
        for ($i = 0; $i < 6; $i++) {
            if ($ackData[$i] == BIN_FLAG_CHAR || $ackData[$i] == BIN_ESCAPE_CHAR) {
                $packet[] = BIN_ESCAPE_CHAR;
                $packet[] = (($ackData[$i] ^ BIN_ESCAPE_CHAR) & 0xFF);
            } else {
                $packet[] = $ackData[$i];
            }
        }
        $packet[] = BIN_FLAG_CHAR;
        return $packet;
    }

    public static function binFrameFormatCheck(&$binFrame)
    {
        $bEscape = false;
        $dataLen = 0;
        for ($i = 0; $i < count($binFrame); $i++) {
            if ($bEscape) {
                $bEscape = false;
                $binFrame[$dataLen++] = ($binFrame[$i] ^ BIN_ESCAPE_CHAR);
            } else {
                if ($binFrame[$i] == BIN_ESCAPE_CHAR) {
                    $bEscape = true;
                } else {
                    $binFrame[$dataLen++] = $binFrame[$i];
                }
            }
        }
        if (self::getCrc16Value($binFrame, $dataLen) != 0) {
            return 0;
        }
        return $dataLen - 2;
    }

    //CRC-CCITT (XModem)
    public static function getCrc16Value($dat, $length)
    {
        $crc_table=array(
        0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
        0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
        0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
        0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
        0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
        0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
        0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
        0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
        0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
        0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
        0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
        0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
        0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
        0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
        0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
        0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
        0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
        0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
        0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
        0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
        0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
        0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
        0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
        0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
        0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
        0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
        0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
        0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
        0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
        0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
        0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
        0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
        );
        $crc = 0;

        for ($i = 0; $i < $length; $i++) {
            $da = $crc >> 8;
            $da &= 0x00000000000000FF;
            $crc <<= 8;
            $crc &= 0x000000000000FFFF;
            $crc ^= $crc_table[$da ^ $dat[$i]];
        }

        return $crc;
    }

    public static function hexStringDec($hexArray)
    {
        if (!ctype_xdigit($hexArray[0])) {
            return $hexArray;
        }
        foreach ($hexArray as $i) {
            $binData[] = hexdec($i);
        }
        return $binData;
    }

    public static function dataBinAcknowledgement($data)
    {
        $crcData = self::getCrc16Value($data, count($data));
        $ackData = array(
        PROTOCOL_VERSION,
        ACK_FLAG,
        (($crcData >> 8) & 0xFF),
        $crcData & 0xFF,
        );
        $crcFrame = self::getCrc16Value($ackData, count($ackData));
        $ackData[] = ($crcFrame >> 8) & 0x00FF;
        $ackData[] = $crcFrame & 0x00FF;
        return self::binDataPacket($ackData);
    }

    public static function binFrameDecode($dat, $len)
    {
        if ($len < 10) {
            //echo "Pacote menor que 10\n";
            return;
        }
        $pos = 0;
        if ($dat[$pos] != PROTOCOL_VERSION) {
            //echo "Can not support protocol version: ". $dat[$pos];
            return;
        }
        $pos++;
        if ($dat[$pos] != 0x01) {
            //echo "Can not support frame NO: ". $dat[$pos];
            return;
        }
        $pos++;
        $deviceID = "";
        for ($i = 0; $i < 8; $i++) {
            if ($i == 0) {
                $deviceID .= $dat[$pos++];
            } else {
                $deviceID .= sprintf("%02X", $dat[$pos++]);
            }
        }
        $array["OBD"] = $deviceID;

        $timeSeconds = self::readUint32($dat, $pos);
        $pos += 4;
        $fix3D = ($timeSeconds & 0x80000000) != 0;
        $timeSeconds &= 0x7FFFFFFF;
        if ($timeSeconds != 0) {
            $date = \DateTime::createFromFormat("d/m/Y H:i:s", "31/12/1999 22:00:00");
            $date->add(new \DateInterval('PT'.$timeSeconds.'S'));

            $array["Date"] = $date->format('d/m/Y H:i:s');
        }
        while ($pos < $len - 2) {
            $infoId = $dat[$pos++];
            $infoLen;
            $array;
            switch ($infoId) {
                case 1://GPS
                    $infoLen = $dat[$pos++];
                    $infoData = array_slice($dat, $pos, $infoLen);
                    $array = array_merge($array, self::gpsDecodeFromBinary($infoData, $fix3D));
                    break;
                case 3://STT
                    $infoLen = $dat[$pos++];
                    $infoData = array_slice($dat, $pos, $infoLen);

                    $status = self::sttDecodeFromBinary($infoData);
                    if ($status) {
                        $array["Status"] = $status;
                    }
                    break;
                case 4://MGR
                    $infoLen = $dat[$pos++];
                    $infoData = array_slice($dat, $pos, $infoLen);
                    $array["Distancia"] = self::mgrDecodeFromBinary($infoData);
                    break;
                case 7://OBD
                    $infoLen = $dat[$pos++];
                    $infoData = array_slice($dat, $pos, $infoLen);
                    $array = array_merge($array, self::obdDecodeFromBinary($infoData));
                    break;
                case 8://FUL
                    $infoLen = $dat[$pos++];
                    $infoLen &= 0x0F;
                    $algorithm = ($infoLen >> 4) & 0x0f;
                    $infoData = array_slice($dat, $pos, $infoLen);
                    self::fulDecodeFromBinary($infoData, $algorithm);
                    break;
                case 9://OAL
                    $infoLen = $dat[$pos++];
                    $infoData = array_slice($dat, $pos, $infoLen);
                    $array = array_merge($array, self::obdDecodeFromBinary($infoData));
                    break;
                case 0x0A://HDB
                    $infoLen = $dat[$pos++];
                    $infoData = array_slice($dat, $pos, $infoLen);
                    $alarm = self::hdbDecodeFromBinary($infoData);
                    if ($alarm) {
                        $array['Alertas'] = $alarm;
                    }
                    break;
                case 0x0B://CAN--j1939 data
                    $infoLen = $dat[$pos++];
                    $infoLen = $infoLen * 256 + $dat[$pos++];
                    $infoData = array_slice($dat, $pos, $infoLen);
                    self::canDecodeFromBinary($infoData);
                    break;
                case 0x0C://HVD--j1708 data
                    $infoLen = $dat[$pos++];
                    $infoData = array_slice($dat, $pos, $infoLen);
                    self::hvdDecodeFromBinary($infoData);
                    break;
                case 0x0D://VIN
                    $infoLen = $dat[$pos++];
                    $infoData = array_slice($dat, $pos, $infoLen);
                    $array = array_merge($array, self::vinDecodeFromBinary($infoData));
                    break;
                case 0x0F://EGT
                    $infoLen = $dat[$pos++];
                    $infoData = array_slice($dat, $pos, $infoLen);
                    $array['Tempo'] = self::egtDecodeFromBinary($infoData);
                    break;
                default:
                    $infoLen = $dat[$pos++];
                    break;
            }
            $pos += $infoLen;
        }
        return $array;
    }

    public static function readInt32($dat, $pos)
    {
        $val = self::readUint32($dat, $pos);
        if ($val & 0x80000000) {
            $val = ~$val & 0x7FFFFFFF;
            $val = -($val+1);
        }
        return $val;
    }

    public static function readUint32($dat, $pos)
    {
        if ($pos + 4 > count($dat)) {
            return 0;
        }
        $val = 0;
        $aux = '';
        for ($i = 0; $i < 4; $i++) {
            $val = ($val << 8) + $dat[$pos + $i];
        }
        return $val;
    }

    public static function readUint16($dat, $pos)
    {
        if ($pos + 2 > count($dat)) {
            return 0;
        }
        $val = 0;
        for ($i = 0; $i < 2; $i++) {
            $val = ($val << 8) + $dat[$pos + $i];
        }
        return $val;
    }

    public static function gpsDecodeFromBinary($info, $is3d)
    {
        if (count($info) != 14) {
            return;
        }

        $array["Latitude"] = self::readInt32($info, 0)/1000000;
        $array["Longitude"] = self::readInt32($info, 4)/1000000;
        $array["Velocidade"] = self::readUInt16($info, 8);
        $array["Angulo"] = self::readUInt16($info, 10);
        $array["HDOP"] = self::readUInt16($info, 12)/100;

        return $array;
    }

    public static function mgrDecodeFromBinary($info)
    {
        if (count($info) != 4) {
            return;
        }
        return self::readUint32($info, 0);
    }

    public static function obdDecodeFromBinary($info)
    {
        return (self::obdDataDecode($info));
    }

    public static function obdDataDecode($obdData)
    {
        $pos = 0;
        $array = [];
        while ($pos < count($obdData)) {
            $len = ($obdData[$pos] >> 4) & 0x0F;
            if ($len + $pos > count($obdData)) {
                break;
            }
            if ($len < 3 || $len > 8) {
                $pos += $len;
                continue;
            }
            $service = ($obdData[$pos] & 0x0F);
            switch ($service) {
                case 1:
                case 2:
                    $pid = $obdData[$pos + 1];
                    $pidValue = array_slice($obdData, $pos + 2, $len-2);
                    $atual =  self::obdService0102Decode($pidValue, $service, $pid);
                    if (count($atual)>0) {
                        $array = array_merge($array, $atual);
                    }
                    break;
                case 3:
                    $value = array_slice($obdData, $pos + 1, $len-1);
                    $erros = self::obdService03Decode($value);
                    if ($erros) {
                        $array['Erros'] = $erros;
                    }
                    break;
                case 9:
                    $pid = $obdData[$pos + 1];
                    $pidValue = array_slice($obdData, $pos + 2, $len-2);
                    $atual =  self::obdService09Decode($pidValue, $pid);
                    if (array_key_exists("VIN", $array)) {
                        $array["VIN"] .= $atual;
                    } else {
                        $array["VIN"] = $atual;
                    }
                    break;
                default:
                    break;
            }
            $pos += $len;
        }
        return $array;
    }

    public static function obdService09Decode($info, $pid)
    {
        //se $pid nao for do vin, não retorna nada
        if ($pid!=2) {
            return;
        }
        $hex = "";
        for ($i = 1; $i<count($info); $i++) {
            if ($info[$i]==255) {
                $hex .= '*';
            } else {
                $hex .= chr($info[$i]);
            }
        }

        return $hex;
    }

    public static function obdService0102Decode($value, $service, $pid)
    {
        $array = [];
        switch ($pid) {
            case 0x04:
                if (count($value)!= 1) {
                    return;
                }
                $clv  = ($value[0]) * 100 / 255;
                $array["Load"] = $clv;
                break;
            case 0x05:
                if (count($value) != 1) {
                    return;
                }
                $ect = $value[0];
                $ect -= 40;

                $array["ECT"] = $ect;
                break;
            case 0x0B:
                if (count($value) != 1) {
                    return;
                }
                $array["MAP"] = $value[0];
                break;
            case 0x0C:
                if (count($value) != 2) {
                    return;
                }
                $rpm = ($value[0] * 256 + $value[1]) / 4;
                $array["RPM"] = $rpm;
                break;
            case 0x0D:
                if (count($value) != 1) {
                    return;
                }
                $speed = $value[0];
                $array["SPD"] = $speed;
                break;
            case 0x0F:
                if (count($value) != 1) {
                    return;
                }
                $iat = $value[0];
                $iat -= 40;
                $array["IAT"] = $iat;
                break;
            case 0x10:
                if (count($value) != 2) {
                    return;
                }
                $maf = (($value[0] * 256 + $value[1])) / 100;
                $array["MAF"] = $maf;
                break;
            case 0x11:
                if (count($value) != 1) {
                    return;
                }
                $position = ($value[0]) * 100 / 255;
                $array["Aceleracao"] = $position;
                break;
            case 0x1F: //run time since engine start
                if (count($value) != 2) {
                    return;
                }
                $position = $value[0] * 256 + $value[1];
                $array["Tempo"] = $position;
                break;
            case 0x2F:
                if (count($value) != 1) {
                    return;
                }
                $percent = $value[0] / 255;
                $array["Combustivel"] = round($percent, 3);
                break;
            case 0x4F:
                if (count($value)!=4) {
                    return;
                }
                $array["MAXOXILAMBDA"] = $value[0] ? $value[0]/65535 : 2/65535;
                $array["MAXOXIVOLT"] = $value[1] ? $value[1]/65535 : 8/65535;
                $array["MAXOXICURRENT"] = $value[2] ? $value[2]/32768 : 128/32768;
                $array["MAXMAP"] = $value[3] ? $value[3]*10/255 : 1;
                break;
        }
        return $array;
    }

    public static function obdService03Decode($value)
    {
        $erros = '';

        $offset = (count($value) % 2 != 0) ? 1 : 0;
        $dtcChars = [ "P", "C", "B", "U" ];
        for ($i = 0; $i < count($value) / 2; $i++) {
            $dtcA = $value[2 * $i + $offset];
            $dtcB = $value[2 * $i + $offset + 1];
            if ($dtcA == 0 && $dtcB == 0) {
                continue;
            }
            $erros .= sprintf(
                "%s%02d%02X,",
                $dtcChars[(($dtcA >> 6) & 0x03)],
                ($dtcA & 0x3F),
                $dtcB
            );
        }
        return substr($erros, 0, -1);
    }

    public static function fulDecodeFromBinary($info, $id)
    {
        if (count($info) != 4) {
            return;
        }
        $fuel = self::readUint32($info, 0);
        return $fuel;
    }

    public static function hdbDecodeFromBinary($info)
    {
        if (count($info) != 1) {
            return;
        }

        $hdb = $info[0] & 0x000F;
        if ($hdb == 0) {
            return;
        }
        //printf("Driver Behavior: \r\n");
        $infoBehavior = [
            "Rapid acceleration",
            "Rough braking",
            "Harsh course",
            "No warmup",
            "Long idle",
            "Fatigue driving",
            "Rough terrain",
            "High RPM"
        ];
        $alarm = '';
        for ($i = 0; $i < 8; $i++) {
            $bitMask = (0x0001 << $i);
            if (($hdb & $bitMask) == 0) {
                continue;
            }
            $alarm .= $infoBehavior[$i] . ", ";
        }
        return $alarm;
    }

    public static function canDecodeFromBinary($info)
    {
        printf("j1939: \r\n");
        self::decodej1939($info);
    }

    public static function decodej1939($candata)
    {
        $pos = 0;
        while ($pos < count($candata)) {
            $len = $candata[$pos];
            if ($len + $pos + 1 > count($candata)) {
                break;
            }
            if ($len < 4 || $candata[$pos + 1] != 0) {
                $pos += $len + 1;
                continue;
            }

            $pgn = self::readUint16($candata, $pos + 2);
            $value = array_slice($candata, $pos+4, $len-3);
            self::j1939PgnDecode($value, $pgn);
            $pos += $len + 1;
        }
    }

    public static function sttDecodeFromBinary($info)
    {
        if (count($info) != 4) {
            return;
        }
        $iStatus = self::readUint16($info, 0);
        $iAlarm = self::readUint16($info, 2);
        $infoStatus = [
            "Power cut",
            "Moving",
            "Over speed",
            "Jamming",
            "Geo-fence alarming",
            "Immobilizer",
            "ACC",
            "Input high level",
            "Input mid level",
            "Engine",
            "Panic",
            "OBD alarm",
            "Course rapid change",
            "Speed rapid change",
            "Roaming",
            "Inter roaming"
        ];
        $infoAlarm = [
            "Power cut",
            "Moved",
            "Over speed",
            "Jamming",
            "Geo-fence",
            "Towing",
            "Reserved",
            "Input low",
            "Input high",
            "Reserved",
            "Panic",
            "OBD",
            "Reserved",
            "Reserved",
            "Accident",
            "Battery low"
        ];
        $status = '';
        for ($i = 0; $i < 16; $i++) {
            $bitMask = (0x0001 << $i);
            if ($iStatus & $bitMask) {
                $status .= $infoStatus[$i] . ', ';
            }
        }
        for ($i = 0; $i < 16; $i++) {
            $bitMask = (0x0001 << $i);
            if ($iAlarm & $bitMask) {
                $status .= $infoAlarm[$i] . ', ';
            }
        }
        return $status;
    }

    public static function j1939PgnDecode($value, $pgn)
    {
        $hex = "";
        for ($i = 0; $i < count($value); $i++) {
            $hex .= sprintf("%02X ", $value[$i]);
        }
        printf(
            "\tPGN%d: %s--- ",
            $pgn,
            $hex
        );

        switch ($pgn) {
            case 61444://(0x00F004)Engine speed
                printf("Electronic Engine Controller 1\r\n");
                printf(
                    "\t%d--Engine Torque Mode\r\n",
                    $value[0] & 0x0F
                );
                printf(
                    "\t%.3f%%--Actual Engine - Percent Torque High Resolution\r\n",
                    0.125 * (($value[0] >> 4) & 0x0F)
                );
                printf(
                    "\t%d--Driver's Demand Engine - Percent Torque\r\n",
                    $value[1]
                );
                printf(
                    "\t%d--Actual Engine - Percent Torque\r\n",
                    $value[2]
                );
                printf(
                    "\t%.3fRPM--Engine speed\r\n",
                    0.125 * self::reverseBytes(self::readUint16($value, 3))
                );
                printf(
                    "\t%d--Source Address of Controlling Device for Engine Control\r\n",
                    $value[5]
                );
                printf(
                    "\t%d--Engine Starter Mode\r\n",
                    $value[6] & 0x0F
                );
                printf(
                    "\t%d%%--Engine Demand – Percent Torque\r\n",
                    -125 + $value[7]
                );
                break;
            case 65132://(0x00FE6C)Vehicle speed
                printf("Tachograph\r\n");
                printf(
                    "\t%.3fkm/h--Vehicle speed\r\n",
                    self::reverseBytes(self::readUint16($value, 6)) / 256
                );
                break;
            case 65217://(0x00FEC1)High Resolution Total Vehicle Distance
                printf("High Resolution Vehicle Distance\r\n");
                printf(
                    "\t%.3fkm--High Resolution Total Vehicle Distance\r\n",
                    self::reverseBytes(self::readUint32($value, 0)) / 200
                );
                break;
            case 65248:
                printf("Vehicle Distance\r\n");
                printf(
                    "\t%.3fkm--Trip Distance\r\n",
                    self::reverseBytes(self::readUint32($value, 0)) / 8
                );
                printf(
                    "\t%.3fkm--Total Vehicle Distance\r\n",
                    self::reverseBytes(self::readUint32($value, 4)) / 8
                );
                break;
            case 65262://(0x00FEEE)Engine Coolant Temperature
                printf("Engine Temperature 1\r\n");
                printf(
                    "\t%ddeg C--Engine Coolant Temperature\r\n",
                    $value[0] - 40
                );
                break;
            case 65253://(0x00FEE5)Engine Hours, Revolutions
                printf("Engine Hours, Revolutions\r\n");
                printf(
                    "\t%2.fH--Engine Total Hours of Operation\r\n",
                    self::reverseBytes(self::readUint32($value, 0)) * 0.05
                );
                printf(
                    "\t%.fr--Engine Total Revolutions\r\n",
                    self::reverseBytes(self::readUint32($value, 4)) * 1000
                );
                break;
            case 65256://(0x00FEE8)Vehicle Direction/Speed
                printf("Vehicle Direction/Speed\r\n");
                printf(
                    "\t%.2fdeg--Compass Bearing\r\n",
                    self::reverseBytes(self::readUint16($value, 0)) / 128
                );
                printf(
                    "\t%.2fkm/h--Navigation-Based Vehicle Speed\r\n",
                    self::reverseBytes(self::readUint16($value, 2)) / 256
                );
                printf(
                    "\t%.2fdeg--Pitch\r\n",
                    self::reverseBytes(self::readUint16($value, 4)) / 128 - 200
                );
                printf(
                    "\t%.2fm--Altitude\r\n",
                    self::reverseBytes(self::readUint16($value, 6)) / 8 - 2500
                );
                break;
            case 65257://(0x00FEE9)Fuel consumption
                printf("Fuel Consumption (Liquid)\r\n");
                printf(
                    "\t%.1fL--Engine trip fuel\r\n",
                    self::reverseBytes(self::readUint32($value, 0)) * 0.5
                );
                printf(
                    "\t%.1fL--Engine total fuel used\r\n",
                    self::reverseBytes(self::readUint32($value, 4)) * 0.5
                );
                break;
            case 61443://(0x00F003)Accelerator Pedal Position
                printf("Electronic Engine Controller 2\r\n");
                printf(
                    "\t%.1f%%--Accelerator Pedal Position\r\n",
                    $value[1] * 0.4
                );
                break;

            case 65263:
                printf("Engine Fluid Level/Pressure 1\r\n");
                printf(
                    "\t%dkPa--Engine Fuel Delivery Pressure\r\n",
                    4 * $value[0]
                );
                printf(
                    "\t%dkPa--Engine Extended Crankcase Blow-by Pressure\r\n",
                    0.05 * $value[1]
                );
                printf(
                    "\t%d%%--Engine Oil Level\r\n",
                    0.4 * $value[ 2]
                );
                printf(
                    "\t%dkPa--Engine Oil Pressure\r\n",
                    4 * $value[ 3]
                );
                printf(
                    "\t%.3fkPa--Engine Crankcase Pressure\r\n",
                    -225 + self::reverseBytes(self::readUint16($value, 4)) / 128
                );
                printf(
                    "\t%dkPa--Engine Coolant Pressure\r\n",
                    2 * $value[ 6]
                );
                printf(
                    "\t%d%%--Engine Coolant Level\r\n",
                    0.4 * $value[ 7]
                );
                break;
            case 65265:
                printf("Cruise Control/Vehicle Speed\r\n");
                printf(
                    "\t%d--Two Speed Axle Switch\r\n",
                    $value[ 0] & 0x03
                );
                printf(
                    "\t%d--Parking Brake Switch\r\n",
                    ($value[ 0] >> 2) & 0x03
                );
                printf(
                    "\t%d--Cruise Control Pause Switch\r\n",
                    ($value[ 0] >> 4) & 0x03
                );
                printf(
                    "\t%d--Park Brake Release Inhibit Request\r\n",
                    ($value[ 0] >> 6) & 0x03
                );
                printf(
                    "\t%.1fkm/h--Wheel-Based Vehicle Speed\r\n",
                    self::reverseBytes(self::readUint16($value, 1)) / 256
                );
                printf(
                    "\t%d--Cruise Control Active\r\n",
                    $value[ 3] & 0x03
                );
                printf(
                    "\t%d--Cruise Control Enable Switch\r\n",
                    ($value[ 3] >> 2) & 0x03
                );
                printf(
                    "\t%d--Brake Switch\r\n",
                    ($value[ 3] >> 4) & 0x03
                );
                printf(
                    "\t%d--Clutch Switch\r\n",
                    ($value[ 3] >> 6) & 0x03
                );
                printf(
                    "\t%d--Cruise Control Set Switch\r\n",
                    $value[ 4] & 0x03
                );
                printf(
                    "\t%d--Cruise Control Coast (Decelerate) Switch\r\n",
                    ($value[ 4] >> 2) & 0x03
                );
                printf(
                    "\t%d--Cruise Control Resume Switch\r\n",
                    ($value[ 4] >> 4) & 0x03
                );
                printf(
                    "\t%d--Cruise Control Accelerate Switch\r\n",
                    ($value[ 4] >> 6) & 0x03
                );
                printf(
                    "\t%dkm/h--Cruise Control Set Speed\r\n",
                    $value[ 5]
                );
                printf(
                    "\t%d--PTO Governor State\r\n",
                    $value[ 6] & 0x1F
                );
                printf(
                    "\t%d--Cruise Control States\r\n",
                    ($value[ 6] >> 5) & 0x07
                );
                printf(
                    "\t%d--Engine Idle Increment Switch\r\n",
                    $value[ 7] & 0x03
                );
                printf(
                    "\t%d--Engine Idle Decrement Switch\r\n",
                    ($value[ 7] >> 2) & 0x03
                );
                printf(
                    "\t%d--Engine Test Mode Switch\r\n",
                    ($value[ 7] >> 4) & 0x03
                );
                printf(
                    "\t%d--Engine Shutdown Override Switchh\r\n",
                    ($value[ 7] >> 6) & 0x03
                );
                break;
            case 65270:
                printf("Inlet/Exhaust Conditions 1\r\n");
                printf(
                    "\t%.1fkPa--Engine Diesel Particulate Filter Inlet Pressure\r\n",
                    0.5 * $value[ 0]
                );
                printf(
                    "\t%dkPa--Engine Intake Manifold #1 Pressure\r\n",
                    2 * $value[ 1]
                );
                printf(
                    "\t%ddeg C--Engine Intake Manifold 1 Temperature\r\n",
                    -40 + $value[ 2]
                );
                printf(
                    "\t%dkPa--Engine Air Inlet Pressure\r\n",
                    2 * $value[ 3]
                );
                printf(
                    "\t%.2fkPa--Engine Air Filter 1 Differential Pressure\r\n",
                    0.05 * $value[ 4]
                );
                printf(
                    "\t%.2fdeg C--Engine Exhaust Gas Temperature\r\n",
                    -273 + 0.03125 * self::reverseBytes(self::readUint16($value, 5))
                );
                printf(
                    "\t%.1fkPa--Engine Coolant Filter Differential Pressure\r\n",
                    0.5 * $value[ 7]
                );
                break;
            case 65271:
                printf("Vehicle Electrical Power 1\r\n");
                printf(
                    "\t%dA--Net Battery Current\r\n",
                    -125 + $value[ 0]
                );
                printf(
                    "\t%dA--Alternator Current\r\n",
                    $value[ 1]
                );
                printf(
                    "\t%.2fV--Charging System Potential (Voltage)\r\n",
                    0.05 *self::reverseBytes(self::readUint16($value, 2))
                );
                printf(
                    "\t%.2fV--Battery Potential / Power Input 1\r\n",
                    0.05 * self::reverseBytes(self::readUint16($value, 4))
                );
                printf(
                    "\t%.2fV--Keyswitch Battery Potential\r\n",
                    0.05 * self::reverseBytes(self::readUint16($value, 6))
                );
                break;
            case 65276:
                printf("(R)Dash Display\r\n");
                printf(
                    "\t%d%%--Washer Fluid Level\r\n",
                    0.4 * $value[ 0]
                );
                printf(
                    "\t%d%%--Fuel Level 1\r\n",
                    0.4 * $value[ 1]
                );
                printf(
                    "\t%dkPa--Engine Fuel Filter Differential Pressure\r\n",
                    2 * $value[ 2]
                );
                printf(
                    "\t%.1fkPa--Engine Oil Filter Differential Pressure\r\n",
                    0.5 * $value[ 3]
                );
                printf(
                    "\t%.2fdeg C--Cargo Ambient Temperature\r\n",
                    -273 + 0.03125 * self::reverseBytes(self::readUint16($value, 4))
                );
                printf(
                    "\t%d%%--Fuel Level 2\r\n",
                    0.4 * $value[6]
                );
                break;
            case 65226://(DM1)Active DTCs and lamp status information
                printf("(DM1)Active DTCs and lamp status information\r\n");
                printf(self::j1939DtcsDecode($value));
                break;
            case 65227://(DM2)Previously active DTCs and lamp status information
                printf("(DM2)Previously active DTCs and lamp status information\r\n");
                printf(self::j1939DtcsDecode($value));
                break;
            default:
                printf("\r\n");
                break;
        }
    }

    public static function reverseBytes($value)
    {
        return ($value & 0x000000FF) << 24 | ($value & 0x0000FF00) << 8 |
                ($value & 0x00FF0000) >> 8 | ($value & 0xFF000000) >> 24;
    }

    public static function j1939DtcsDecode($value)
    {
        if (count($value) < 7) {
            return;
        }
        $lampStatus = ["OFF", "ON","Unknown", "Unknown"];

        printf("\tMIL: ");
        printf($lampStatus[($value[0] >> 6) & 0x03] . "\r\n");

        printf("\tRSL: ");
        printf($lampStatus[($value[0] >> 4) & 0x03] . "\r\n");

        printf("\tAWL: ");
        printf($lampStatus[($value[0] >> 2) & 0x03] . "\r\n");

        printf("\tPL: ");
        printf($lampStatus[$value[0] & 0x03] . "\r\n");

        for ($i = 0; $i < intdiv((count($value) - 2), 4); $i++) {
            printf("              DTC%d:\r\n", $i);
            $SPN = $value[2 + 4 * $i + 2] >> 5;
            $SPN = ($SPN << 8) + $value[2 + 4 * $i + 1];
            $SPN = ($SPN << 8) + $value[2 + 4 * $i];
            $FMI = $value[2 + 4 * $i + 2] & 0x1F;
            $OC = $value[2 + 4 * $i + 3] & 0x7F;
            printf("\tSPN: %d\r\n", $SPN);
            printf("\tFMI: %d\r\n", $FMI);
            printf("\tOC: %d\r\n", $OC);
        }
    }

    public static function hvdDecodeFromBinary($info)
    {
        printf("j1708:\r\n");
        self::j1708DataDecode($info);
    }

    public static function j1708DataDecode($hvddata)
    {
        $pos = 0;
        while ($pos < count($hvddata)) {
            $len = $hvddata[$pos] & 0x3F;
            $paratype = $hvddata[$pos] >> 6 & 0x03;
            if ($len + $pos > count($hvddata)) {
                break;
            }
            if ($len < 2 || $len > 22 || $paratype == 0) {
                $pos += $len + 1;
                continue;
            }
            if ($paratype == 1) {//MID data
                $value = array_slice($hvddata, $pos + 1, $len);
                self::j1708MidDecode($value);
            } else {
                $j1587pid = $hvddata[$pos + 1];
                if ($paratype == 3) {
                    $j1587pid += 256;
                }
                $value = array_slice($hvddata, $pos + 2, $len-1);
                self::j1587PidDecode($value, $j1587pid);
            }
            $pos += $len + 1;
        }
    }

    public static function j1708MidDecode($value)
    {
        $hex = "";
        for ($i = 1; $i < count($value); $i++) {
            $hex .= sprintf("%2X ", $value[$i]);
        }
        printf(
            "\tMID%d: %s\r\n",
            $value[0],
            $hex
        );
    }

    public static function j1587PidDecode($value, $pid)
    {
        switch ($pid) {
            case 84://Road speed
                printf(
                    "        PID%d: %.3fkm/h--Road speed\r\n",
                    $pid,
                    $value[0] * 0.805
                );
                break;
            case 96://Fuel level
                printf(
                    "        PID%d: %.2f%%--Fuel level\r\n",
                    $pid,
                    $value[0] * 0.5
                );
                break;
            case 110://Engine Coolant Temperature
                printf(
                    "        PID%d: %dFahrenheit--Engine Coolant Temperature\r\n",
                    $pid,
                    $value[0]
                );
                break;
            case 190://Engine speed
                printf(
                    "        PID%d: %.2fRPM--Engine speed\r\n",
                    $pid,
                    self::readUint16($value, 0) * 0.25
                );
                break;
            case 245://Total Vehicle Distance
                printf(
                    "        PID%d: %.3fkm--Total Vehicle Distance\r\n",
                    $pid,
                    self::readUint32($value, 0) * 0.161
                );
                break;
            default:
                $hex = "";
                for ($i = 0; $i < count($value); $i++) {
                    $hex .= sprintf("%2X ", $value[$i]);
                }
                printf(
                    "        PID%d: %s\r\n",
                    $pid,
                    $hex
                );
                break;
        }
    }

    public static function vinDecodeFromBinary($info)
    {
        $hex = "";
        for ($i = 0; $i<count($info); $i++) {
            $hex .= chr($info[$i]);
        }
        return array("Chassi"=>$hex);
    }

    public static function hex2str($hex)
    {
        $str = "";
        for ($i=0; $i<count($hex); $i+=2) {
            $str .= chr(hexdec($hex[$i]));
        }
        return $str;
    }

    public static function egtDecodeFromBinary($info)
    {
        if (count($info)!= 4) {
            return;
        }
        return self::readUint32($info, 0);
    }
}
