<?php

namespace Command;

class Connection
{
    private const TCP_MAX_CONNECTIONS = 90;
    private const TCP_TIMEOUT_SEC = 300;
    private const TCP_HOST = 'localhost';
    private const TCP_PORT = 38000;

    private const DB_HOST = 'localhost';
    private const DB_NAME = 'chat_sis';
    private const DB_USER = 'root';
    private const DB_PASSWORD = 'root';

    /**
     * @var \Doctrine\DBAL\DriverManager
     */
    private $connection;

    private $user = [
        '353323081610395' =>1,
        '861107031366629' =>2,
        '868323029968580' =>3,
        '868323029974083' =>4,
        '868323029977383' =>5,
    ];

    private $obd = [
        '353323081610395' =>1,
        '861107031366629' =>2,
        '868323029968580' =>3,
        '868323029974083' =>4,
        '868323029977383' =>5,
    ];

    public function __construct($param = null)
    {
        $this->connection = \Doctrine\DBAL\DriverManager::getConnection([
            'host' => self::DB_HOST,
            'dbname' => self::DB_NAME,
            'user' => self::DB_USER,
            'password' => self::DB_PASSWORD,
            'driver' => 'pdo_mysql',
        ], (new \Doctrine\DBAL\Configuration()));

        $this->clientBroadcast = new \ClientNotifier();
    }

    /*
    *   Essa função cria um socket de baixo nível e o configura para obter dados
    *   de n conexões diferentes
    */
    public function execute()
    {
        // Ignore user aborts and allow the script to run forever
        ignore_user_abort(true);
        set_time_limit(0);

        if (!($server = socket_create(AF_INET, SOCK_STREAM, SOL_TCP))) {
            $errorcode = socket_last_error();
            $errormsg = socket_strerror($errorcode);

            die("Couldn't create socket: [$errorcode] $errormsg \n");
        }

        // set the option to reuse the port
        socket_set_option($server, SOL_SOCKET, SO_REUSEADDR, 1);

        // Bind the source address
        if (!socket_bind($server, self::TCP_HOST, self::TCP_PORT)) {
            $errorcode = socket_last_error();
            $errormsg = socket_strerror($errorcode);

            die("Could not bind socket : [$errorcode] $errormsg \n");
        }


        if (!socket_listen($server, self::TCP_MAX_CONNECTIONS)) {
            $errorcode = socket_last_error();
            $errormsg = socket_strerror($errorcode);

            die("Could not listen on socket : [$errorcode] $errormsg \n");
        }

        $clients = [$server];

        //esse arranjo $time coleta o tempo da última atualização
        $time = [0];

        //o primeiro termo é removido por representar a variável $server
        unset($time[0]);

        //start loop to listen for incoming connections
        while (true) {
            // create a copy, so $clients doesn't get modified by socket_select()
            $read = $clients;
            $write = null;
            $except = null;
            // get a list of all the clients that have data to be read from
            // if there are no clients with data, block until it finds a new client
            socket_select($read, $write, $except, null);

            // check if there is a new client trying to connect
            if (in_array($server, $read)) {
                // accept the client
                $newsock = socket_accept($server);

                try {
                    socket_getpeername($newsock, $ip);
                    $clients[] = $newsock;
                    $time[] = time();
                    // show number of clients
                    echo "New client connected: {$ip}\n";
                    echo "There are ".(count($clients) - 1)." client(s) connected to the server\n";
                } catch (\Exception $e) {
                    continue;
                }
                // remove the listening socket from the clients-with-data array
                $key = array_search($server, $read);
                unset($read[$key]);
            }

            // loop through all the clients that have data to read from
            foreach ($read as $key => $read_sock) {
                $data = null;
                try {
                    // read until newline or 8192 bytes
                    socket_recv($read_sock, $data, 8192, MSG_DONTWAIT);
                } catch (\Exception $e) {
                    // remove client for $clients array
                    $clients[$key] = null;
                    $time[$key] = null;
                    unset($clients[$key]);
                    unset($time[$key]);
                    echo "client disconnected.\n";
                    gc_collect_cycles();
                    // continue to the next client to read from, if any
                    continue;
                }

                // check if the client is disconnected
                if (!$data) {
                    // remove client for $clients array
                    $clients[$key] = null;
                    $time[$key] = null;
                    unset($clients[$key]);
                    unset($time[$key]);
                    echo "client disconnected!\n";
                    gc_collect_cycles();
                    // continue to the next client to read from, if any
                    continue;
                }

                // Abrindo um pacote de OBD com 1 ou mais logs
                $package = \DecoderArray::dataDecode(trim($data));

                if (!empty($package)) {
                    //aqui temos uma mensagem recebida
                    //sendo necessário persisti-la num BD
                    foreach ($package as $data) {
                        if (empty($data['Latitude']) || empty($data['Longitude'])) {
                            continue;
                        }

                        try {
                            $data = [
                                'date' => date('Y-m-d H:i:s'),
                                'latitude' => $data['Latitude'],
                                'longitude' => $data['Longitude'],
                                'user_id' => $this->user[$data['OBD']],
                                'obd_id' => $this->obd[$data['OBD']],
                            ];

                            // Emitindo a mensagem para o socket de broadcast
                            (new \ClientNotifier())->connect('localhost', 3000, 'obd', json_encode($data));

                            // Armazenando dados em base de dados MySQL
                            $this->connection->insert('obd_checkpoint', $data);

                            print('Entrada adicionada ao BD em '.(date('d-m-Y H:i:s'))." \n");
                        } catch (\Exception $e) {
                            print("error".$e->getMessage()."\n");
                            sleep(2);
                        }
                    }
                }
            } // end of reading foreach

            $read = null;
            foreach ($time as $key => $ultimaInsercao) {
                $tempoAtual = time();
                if (($tempoAtual - $ultimaInsercao) >self::TCP_TIMEOUT_SEC) {
                    echo "client timed out \n";
                    $clients[$key] = null;
                    $time[$key] = null;
                    unset($clients[$key]);
                    unset($time[$key]);
                    gc_collect_cycles();
                }
            }
        } //end while
    }
}
