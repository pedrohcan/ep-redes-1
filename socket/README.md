buslab.nodejs.server
======================

#### Requisitos
É necessário instalar o npm e o node.js, versões utilizadas
```
Npm 6.2.0
Node v10.8.0
```

## Instalação para desenvolvimento

#### Instalação de pacotes
Logo após clonar o repositório, rodar instalação de pacotes com o npm
```
npm install
```

#### Rodar servidor
Pode-se utilizar o próprio node para levantar o servidor
```
node index.js
```

Ou pode-se utilizar o nodemon, caso você NÃO TENHA INSTALADO ainda, para alterar o código e não precisar reiniciar o servidor toda vez
```
sudo npm install -g nodemon
```
Para inciar o servidor
```
nodemon index.js
```

## Instalação para produção

#### Instalação de pacotes
Logo após clonar o repositório, rodar instalação de pacotes com o npm
```
npm install --production
```

#### Para iniciar o servidor
Caso ainda NÃO TENHA INSTALADO, utilizar o pacote forever do npm
```
sudo npm install -g forever
```
Para iniciar o servidor em background
```
forever start -w index.js
```
#### Para derrubar o servidor
Listar processos do forever
```
forever list
```
Parar processo da pilha
```
forever stop <indice-da-pilha>
```
