var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;
var clients = [];

server.listen(port);

var users = {};

io.on('connection', function (socket) {
    console.log('Connected', socket.id);

    socket.on('obd', function (data) {
        socket.broadcast.emit('notify-position', data);
    });

    socket.on('message', function (data) {
        users[socket.id] = data.id;

        socket.broadcast.emit('notify-users', Object.values(users));
    });

    socket.on('disconnect', function () {
        delete users[socket.id];

        socket.broadcast.emit('notify-users', Object.values(users));

        console.log('Disconnected', socket.id);
    });
});
