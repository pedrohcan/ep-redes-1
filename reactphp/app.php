<?php

set_time_limit(0);

require __DIR__ . '/vendor/autoload.php';

$sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
socket_connect($sock, 'localhost', 38000);

$loop = \React\EventLoop\Factory::create();

$list = [
    __DIR__ .'/../datasets/353323081610395.csv',
    __DIR__ .'/../datasets/861107031366629.csv',
    __DIR__ .'/../datasets/868323029968580.csv',
    // __DIR__ .'/../datasets/868323029974083.csv',
    // __DIR__ .'/../datasets/868323029977383.csv',
    // __DIR__ .'/../datasets/868323029984249.csv',
];

$timers = [];

foreach ($list as $file) {
    $stream = new \React\Stream\ReadableResourceStream(fopen($file, 'r'), $loop);
    $stream->on('data', function ($data) use ($file, $loop, $sock) {
        echo $file. "\n";

        $lines = explode("\n", $data);
        foreach ($lines as $line) {
            echo 'Reading line'. "\n";
            $row = explode("\t", $line);
            if (count($row) < 3) {
                echo 'Line skiped'. "\n";
                continue;
            }

            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $row[1]);
            if (!is_object($date)) {
                echo 'Invalid date'. "\n";
                continue;
            }

            $obd = $row[0];
            $content = $row[2];

            if (!isset($timers[$file]) || empty($timers[$file])) {
                $time = 0;
            } else {
                $difference = $date->diff($timers[$file]);
                $time = $difference->s
                    + ($difference->i * 60)
                    + ($difference->h * 60 * 60)
                    + ($difference->d * 60 * 60 * 24)
                    + ($difference->m * 60 * 60 * 24 * 30)
                    + ($difference->y * 60 * 60 * 24 * 30 * 12);
            }

            $timers[$file] = $date;

            $loop->addTimer($time, function () use ($obd, $date, $content, $sock) {
                echo 'Sending OBD data: ' . $obd .' => '. $date->format('d/m/Y H:i:s') . "\n";

                socket_send($sock, $content, strlen($content), 0);
            });
        }
    });
}

$loop->run();
