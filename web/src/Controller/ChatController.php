<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ChatController extends AbstractController
{
    /**
     * @Route("/chat", name="chat")
     */
    public function index()
    {
        $user = $this->getDoctrine()
            ->getRepository(\App\Entity\User::class)
            ->findOneByUsername($this->getUser()->getUsername())
        ;

        $contacts = $this->getDoctrine()
            ->getRepository(\App\Entity\User::class)
            ->findFriends($user)
        ;

        return $this->render('chat/index.html.twig', [
            'user' => $user,
            'page_title' => 'Chat',
            'contacts' => $contacts,
        ]);
    }

    /**
     * @Route("/map", name="map")
     */
    public function map()
    {
        $user = $this->getDoctrine()
            ->getRepository(\App\Entity\User::class)
            ->findOneByUsername($this->getUser()->getUsername())
        ;

        return $this->render('chat/map.html.twig', [
            'user' => $user,
            'page_title' => 'Mapa',
            'google_maps_api_key' => 'AIzaSyAHtN6fZEFDaxCxjf9kkvhIlWnAT2HOiDA'
        ]);
    }
}
