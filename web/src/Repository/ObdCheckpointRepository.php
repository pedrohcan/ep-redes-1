<?php

namespace App\Repository;

use App\Entity\ObdCheckpoint;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ObdCheckpoint|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObdCheckpoint|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObdCheckpoint[]    findAll()
 * @method ObdCheckpoint[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObdCheckpointRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ObdCheckpoint::class);
    }

//    /**
//     * @return ObdCheckpoint[] Returns an array of ObdCheckpoint objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObdCheckpoint
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
