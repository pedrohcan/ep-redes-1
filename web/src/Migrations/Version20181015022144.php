<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181015022144 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE obd (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, serial VARCHAR(15) NOT NULL, UNIQUE INDEX UNIQ_A1DEF96BA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE obd_checkpoint (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, obd_id INT DEFAULT NULL, date DATETIME NOT NULL, latitude DOUBLE PRECISION NOT NULL, longitude DOUBLE PRECISION NOT NULL, INDEX IDX_49C5CC50A76ED395 (user_id), INDEX IDX_49C5CC5086D64477 (obd_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(90) NOT NULL, username VARCHAR(90) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE obd ADD CONSTRAINT FK_A1DEF96BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE obd_checkpoint ADD CONSTRAINT FK_49C5CC50A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE obd_checkpoint ADD CONSTRAINT FK_49C5CC5086D64477 FOREIGN KEY (obd_id) REFERENCES obd (id)');

        $this->addSql("INSERT INTO `user` (id, name, username) VALUES
        (1,'Pedro','pedro'),
        (2,'Joao','joao'),
        (3,'Anderson','anderson'),
        (4,'Jordana','jordana'),
        (5,'Admin','admin');");

        $this->addSql("INSERT INTO obd (user_id, serial) VALUES
        (1, '353323081610395'),
        (2, '861107031366629'),
        (3, '868323029968580'),
        (4, '868323029974083'),
        (5, '868323029977383');");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE obd_checkpoint DROP FOREIGN KEY FK_49C5CC5086D64477');
        $this->addSql('ALTER TABLE obd DROP FOREIGN KEY FK_A1DEF96BA76ED395');
        $this->addSql('ALTER TABLE obd_checkpoint DROP FOREIGN KEY FK_49C5CC50A76ED395');
        $this->addSql('DROP TABLE obd');
        $this->addSql('DROP TABLE obd_checkpoint');
        $this->addSql('DROP TABLE user');
    }
}
