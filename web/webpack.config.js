var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')

    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .enableSassLoader()
    .addEntry('app', [
        './assets/template/bower_components/bootstrap-daterangepicker/daterangepicker.css',
        './assets/template/bower_components/dropzone/dist/dropzone.css',
        './assets/template/bower_components/fullcalendar/dist/fullcalendar.min.css',
        './assets/template/icon_fonts_assets/font-awesome/css/font-awesome.min.css',
        './assets/template/css/main.css',
        './assets/template/bower_components/chart.js/dist/Chart.min.js',
        './assets/template/bower_components/bootstrap-validator/dist/validator.min.js',
        './assets/template/bower_components/bootstrap-daterangepicker/daterangepicker.js',
        './assets/template/bower_components/dropzone/dist/dropzone.js',
        './assets/template/bower_components/fullcalendar/dist/fullcalendar.min.js',
        './assets/template/bower_components/tether/dist/js/tether.min.js',
        './assets/template/js/main.js',
        './assets/js/vendors/peer.min.js',
        './assets/js/app.js',
        './assets/js/chat.js'
    ])

    .autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();
