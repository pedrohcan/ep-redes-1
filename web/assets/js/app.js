import 'bootstrap';
import 'select2';

require('../css/app.css');

var $ = require('jquery');

require('./vendors/jquery.maskedinput.min.js');

(function ($){
    $("document").ready(function () {
        $('.cpf').mask('999.999.999-99');
        $('.cpf-optional').mask('?999.999.999-99');

        $('.cnpj').mask('99.999.999/9999-99?9').on("change blur", function(){
            var value = $.trim($(this).val().replace(/\D+/g, ''));
            $(this).mask(($(this).val().length == 14 ? '99.999.999/9999-99?9' : '999.999.999/9999-9?9'));
        });
        $('.cnpj-optional').mask('?999.999.999/9999-99').on("change blur", function(){
            var value = $.trim($(this).val().replace(/\D+/g, ''));
            $(this).mask(value.length == 14 ? '?99.999.999/9999-999' : '?999.999.999/9999-99');
        });

        $('.cep').mask('99999-999');
        $('.cep-optional').mask('?99999-999');

        $('.telefone').mask('(99) 9999-9999');
        $('.celular').mask('(99) 99999-9999');

        $('.telefone-celular').mask('(99) 9999-9999?9').on("change", function(){
            $(this).mask(($(this).val().length < 15 ? '(99) 9999-9999?9' : '(99) 99999-999?9'));
        });

        $('.date').mask('99/99/9999');
        $('.date-optional').mask('?99/99/9999');

        $('.integer,.numeric').each(function(){
            var $this = $(this),
                max = $this.attr('maxlength');

            if (max && max > 0) {
                $this.mask('?' + ("9".repeat(max)), {
                    placeholder: ""
                });
            }
        });

        // $(".money").maskMoney({
        //     prefix:'R$ ',
        //     allowNegative: true,
        //     thousands:'.',
        //     decimal:',',
        //     affixesStay: false,
        //     allowZero: true,
        // });
    })
})($);

String.prototype.formatDateBR = function(){
    var date = new Date(Date.parse(this));
    if (!date instanceof Date) {
        return null;
    }

    var hours = (date.getHours() > 9 ? date.getHours() : '0' + date.getHours());

    return date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear() + " " + hours + ":" + date.getMinutes() + ":" + date.getSeconds();
};
