import io from 'socket.io-client';

Date.prototype.formatDateBR = function(){
    var date = this,
        hours = (date.getHours() > 9 ? date.getHours() : '0' + date.getHours()),
        minutes = (date.getMinutes() > 9 ? date.getMinutes() : '0' + date.getMinutes()),
        seconds = (date.getSeconds() > 9 ? date.getSeconds() : '0' + date.getSeconds()),
        day = date.getDate() > 9 ? date.getDate() : '0' + date.getDate(),
        month = (date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1);

    return day + "/" + month + "/" + date.getFullYear() + " " + hours + ":" + minutes + ":" + seconds;
};

var $ = require('jquery');

var me = null;// Main chat socket
var userId = null;
var connection = null;

var peers = {};
var connections = {};
var currentContact = null;

var socket = io('http://localhost:3000/');


function initPeer(channel)
{
    if (peers[channel]) {
        var peer = peers[channel];
    } else {
        var peer = new Peer(channel, {
            host: 'localhost',
            port: 9000,
            path: '/'
        });

        peers[channel] = peer;
    }

    peer.on('open', function (id) {
        console.log('ID: ' + id);
        console.log("Awaiting connection... (user)");
    });

    peer.on('error', function (err) {
        console.log('' + err);

        if (err.type === 'peer-unavailable') {
            notifyOfflineUser(err.message.split("Could not connect to peer ")[1]);
        }
    });

    peer.on('disconnected', function () {
        console.log("Connection has been lost. (user)");
    });

    return peer;
}

function initConn(peer, callback)
{
    peer.on('connection', function (conn) {
        console.log("Connected on " + peer.id);

        conn.on('data', function (data) {
            console.log("Data received: ", data);

            callback(data);
        });

        conn.on('close', function () {
            console.log("Connection reset: Awaiting connection...");
        });
    });
}

function connect(peer, channel, contactId, callback)
{
    var conn = peer.connect(channel, {
        reliable: true
    });

    conn.on('open', function () {
        console.log("Connection established on " + channel + ": Can send message");

        if (contactId && callback) {
            callback(conn, contactId, peer);
        }
    });

    return conn;
}

function initHandshake(contactId)
{
    // Init my side of the peer chat
    var peer = initPeer('chat-' + contactId + 'x' + me.id);
    initConn(peer, function (data){
        console.log('Left');
        addMessageOnScreen(data, false);
    });

    // Let user know I want to talk to him
    connect(peer, contactId, contactId, function (conn, contactId, peer) {
        conn.send({
            'type': 'handshake',
            'from': me.id,
            'to': contactId,
            'connect_me_on': peer.id,
        });
    });

    peers[contactId] = peer;
};

function acceptHandshake(data)
{
    // Init my side of the peer chat
    if (!peers[data.from]) {
        peers[data.from] = initPeer('chat-' + data.from + 'x' + me.id);

        initConn(peers[data.from], function (data){
            console.log('Right');
            addMessageOnScreen(data, false);
        });
    }

    // Let user know I accepted his request to to talk to him
    connect(peers[data.from], data.from, data.from, function (conn, contactId, peer) {
        conn.send({
            'type': 'accept-handshake',
            'from': me.id,
            'to': contactId,
            'connect_me_on': peer.id,
        });
    });

    connections[data.from] = connect(me, data.connect_me_on);
};

function addMessageOnScreen(data, isSelf)
{
    if (!isSelf) {
        var $contact = $('.user-' + data.from);

        if (!$contact.hasClass('active')) {
            var count = parseInt($contact.find('.user-message-count').html()) + 1;
            $contact.find('.user-message-count').html(count);
        }
    }

    $("#chat-screen-" + (isSelf ? data.to : data.from)).find('.chat-content').append(''
        + '<div class="chat-message ' + (isSelf ? 'self' : '') + '">'
            + '<div class="chat-message-content-w">'
                + '<div class="chat-message-content">'
                    + data.message
                + '</div>'
            + '</div>'
            + '<div class="chat-message-date">'
                + data.now_formatted
            + '</div>'
        + '</div>'
    );
}

function loading(open)
{
    if (open) {
        $("#loading").modal();
    } else {
        $("#loading").modal('hide')
    }
};

function notifyOfflineUser(channel)
{
    loading(false);

    var username = $("#chat-screen-" + channel).find('.contact-name').html();

    $("#offline").find('.username').html(username);
    $("#offline").modal();

    $("#chat-screen-no-user").addClass('active');
    $(".tab-pane.active").removeClass('active');
};

(function ($) {
    $('document').ready(function(){
        userId = $(".full-chat-w").data('user');

        me = initPeer($(".full-chat-w").data('user'));

        initConn(me, function (data) {
            if (typeof data === 'object') {
                if (data.type === 'handshake') {
                    acceptHandshake(data);
                } else if (data.type === 'accept-handshake') {
                    loading(false);
                    connections[data.from] = connect(me, data.connect_me_on);
                }
            }
        });

        socket.on('connect', function () {
            socket.send({
                id: userId,
            });

            socket.on('notify-users', function (data) {
                $(".user-w").each(function(k, e){
                    if (data.includes($(e).data('contact'))) {
                        $(e).find('.online').show();
                        $(e).find('.offline').hide();
                    } else {
                        $(e).find('.online').hide();
                        $(e).find('.offline').show();
                    }
                });
            });
        });

        setInterval(function(){
            socket.send({
                id: userId,
            });
        }, 3 * 1000);
    });

    $(".user-w").on("click", function () {
        socket.send({
            id: userId,
        });

        var $this = $(this),
            name = $this.data('name'),
            contactId = $this.data('contact');

        var $contact = $('.user-' + contactId);
        $contact.find('.user-message-count').html(0);

        currentContact = contactId;

        if (!connections[currentContact]) {
            loading(true);
            initHandshake(currentContact);
        }
    });

    $('.chat-btn a').on('click', function () {
        add_full_chat_message($(this).parents('.chat-controls').find('.chat-input input'));
        return false;
    });

    $('.chat-input input').on('keypress', function (e) {
        if (e.which == 13) {
            add_full_chat_message($(this));
            return false;
        }
    });

    function add_full_chat_message($input) {
        socket.send({
            id: userId,
        });

        var date = new Date();

        var data = {
            peer: connections[currentContact].peer,
            from: me.id,
            to: currentContact,
            message: $input.val(),
            now: date,
            now_formatted: date.formatDateBR(),
        }

        addMessageOnScreen(data, true);
        connections[currentContact].send(data);

        $input.val('');
        // $('.chat-content-w').scrollTop($('.chat-content-w')[0].scrollHeight);
        $('.chat-content-w').scrollTop(1000000);
    }
})(jQuery);
